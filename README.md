#   PFASDR

## Status

[![pipeline status](https://gitlab.com/pfasdr/code/sampler/badges/master/pipeline.svg)](https://gitlab.com/pfasdr/code/sampler/pipelines)
[![coverage report](https://gitlab.com/pfasdr/code/sampler/badges/master/coverage.svg)](https://gitlab.com/pfasdr/code/sampler/-/jobs)

##  OS-level Setup

Ubuntu:

* python3.5-tk
* python3.6-tk
* libgl1-mesa-glx

install them using:

    $ python3 setup.py bootstrap_ubuntu

Windows

*   Go to the releases page on python.org:
    <https://www.python.org/downloads/windows/>.
*   Download the x64 version of the latest Python 3:
    "Windows x86-64 executable installer"
*   Start the installer and choose default options:
    "Install Now"

##  Virtualenv-level Setup

    $ python3.8 -m venv venv
    $ venv/bin/python -m pip install --upgrade pip tox wheel

##  Run Individual Tests

    $ python -m pytest pfasdr/grid/manual/

##  Run Main

    $ PYTHONPATH=. python pfasdr/main/main.py

##  Remove Large Files

     java -Xms512m -Xmx16g -jar ~/Downloads/bfg-1.13.0.jar --massive-non-file-objects-sized-up-to 25M --strip-blobs-bigger-than 30M
     git reflog expire --expire=now --all && git gc --prune=now --aggressive

##  Repository from Directory

    cd ..
    cp -R PFASDR.Code.Main PFASDR.Subproject
    cd PFASDR.Subproject
    java -jar ~/Downloads/bfg-1.13.0.jar --massive-non-file-objects-sized-up-to 25M --delete-folders ./<out of scope directory> --no-blob-protection
    git reflog expire --expire=now --all && git gc --prune=now --aggressive
    git reset HEAD
    rm -rf ./<out of scope directory>

## Installing Compile Time Prerequisites

    sudo apt install --yes libblas-dev liblapack-dev libatlas-base-dev

## Create Runtime Environment

    source venv/bin/activate
     tox -e py37-neural-rocm

## Freeze Runtime Environment Requirements

    .tox/py37-neural-rocm/bin/python -m \
        pip freeze --all --exclude-editable > requirements.d/ze.txt

## Using the Command Line Interface (CLI)

    $ .tox/py37-neural-rocm/bin/python ./pfasdr/cli/main.py sample linear_1
    Sampling grid:  linear_1
    [...]
    $ .tox/py37-neural-rocm/bin/python ./pfasdr/cli/main.py learn linear_1
    Learning grid: linear_1
    [...]
    $ .tox/py37-neural-rocm/bin/python ./pfasdr/cli/main.py monitor
    Learning monitoring learning process ...
    [...]
    $ .tox/py37-neural-rocm/bin/python ./pfasdr/cli/main.py test
    Testing the code base against the test suite ...
    [...]

## Building the Docker Image

    docker build -t registry.gitlab.com/pfasdr/code/sampler .

## Publishing the Docker Image

    docker push registry.gitlab.com/pfasdr/code/sampler

## Using the Docker Image

    docker run --rm -it registry.gitlab.com/pfasdr/code/sampler:latest
