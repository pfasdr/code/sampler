import functools
import multiprocessing
from itertools import combinations
import os

import matplotlib.pyplot as plt

from pfasdr.grid_gridcal.sampler.c_plot_samples_util import get_valid_powers_from_situations, \
    get_invalid_powers_from_situations
from pfasdr.util.paths import DYNSIM_GRAPH_DATE_PATH


from pfasdr.grid_gridcal.samples.plot.c_plot_samples_util import \
    get_test_powers_from_fake_data



def _main():
    print('Parsing valid powers ...')
    powers_valid = get_valid_powers_from_situations(limit=1_000_000)
    sample_count_valid = len(powers_valid)
    print('Valid sample count', sample_count_valid)

    print('Parsing invalid powers ...')
    powers_invalid = get_invalid_powers_from_situations(limit=sample_count_valid)
    sample_count_invalid = len(powers_invalid)
    print('Invalid sample count', sample_count_valid)

    print('Parsing test powers ...')
    powers_test = get_test_powers_from_fake_data(limit=sample_count_valid)
    sample_count_test = len(powers_test)
    print('Test sample count', sample_count_test)

    # Create a dictionary of column-wise lists per validity for matplotlib
    units_powers_valid = dict()
    units_powers_invalid = dict()
    units_powers_test = dict()
    for units_powers, powers in zip([units_powers_valid, units_powers_invalid, units_powers_test],
                                    [powers_valid, powers_invalid, powers_test]):
        for unit_index in range(powers.shape[1]):
            units_powers[unit_index] = powers[powers.iloc[0].index[unit_index]].tolist()
    del powers_valid, powers_invalid, powers_test

    marker_size = (1080 * 1080) / min(sample_count_valid, sample_count_invalid)
    alpha = 0.1

    unit_count = len(units_powers_valid.keys())
    print('Unit count:', unit_count)

    # Run the actual plotting on a process pool
    combination_index_combinations = [
        (combination_index, index_combination)
        for combination_index, index_combination
        in enumerate(combinations(range(unit_count), r=2))]
    function_partial = functools.partial(
        _plot_powers,
        alpha=alpha,
        marker_size=marker_size,
        units_powers_invalid=units_powers_invalid,
        units_powers_valid=units_powers_valid,
        units_powers_test=units_powers_test)
    multiprocessing.Pool(int(multiprocessing.cpu_count() / 2)) \
        .map(function_partial, combination_index_combinations)


def _plot_powers(combination_index_combination, alpha, marker_size,
                 units_powers_invalid, units_powers_valid, units_powers_test):
    # unpack non-partial parameter
    combination_index = combination_index_combination[0]
    index_combination = combination_index_combination[1]
    del combination_index_combination

    print(f'Plotting combination #{combination_index} {index_combination}')

    figure = plt.figure()
    figure.set_size_inches(10.8, 10.8)
    ax = figure.add_subplot(111)

    for color, validity, powers in zip(('crimson', 'blue', 'orange'),
                                       ('invalid', 'valid', 'test'),
                                       (units_powers_invalid, units_powers_valid, units_powers_test)):
        print(f'Plotting {validity} samples ...')

        powers_x = powers[index_combination[0]]
        powers_y = powers[index_combination[1]]

        alpha_now = alpha if validity != 'test' else alpha * 10
        marker_size_now = marker_size if validity != 'test' else marker_size * 10
        ax.scatter(powers_x, powers_y,
                   s=marker_size_now, c=color, alpha=alpha_now,
                   marker="o", edgecolor='None')

    plt.xlabel(s=f'unit {index_combination[0]}')
    plt.ylabel(s=f'unit {index_combination[1]}')
    plt.tight_layout()
    plt.savefig(os.path.join(DYNSIM_GRAPH_DATE_PATH,
                             f'{combination_index}_{index_combination}.png'))
    plt.close(figure)


if __name__ == '__main__':
    _main()
