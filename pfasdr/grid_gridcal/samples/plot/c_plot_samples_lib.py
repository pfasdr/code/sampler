import functools
import multiprocessing
from itertools import combinations
import os

import matplotlib.pyplot as plt

from pfasdr.util.paths import LAST_TRAINING_GRAPHS_PATH


def plot_by_powers_dataframes(powers_invalid, powers_test, powers_valid):
    # Create a dictionary of column-wise lists per validity for matplotlib

    units_powers_valid = {unit_index: power_dict_entry for
                          unit_index, power_dict_entry in
                          get_dict_from_dataframe(powers_valid)}

    units_powers_invalid = {unit_index: power_dict_entry for
                            unit_index, power_dict_entry in
                            get_dict_from_dataframe(powers_invalid)}

    units_powers_test = {unit_index: power_dict_entry for
                         unit_index, power_dict_entry in
                         get_dict_from_dataframe(powers_test)}

    _plot_powers_in_parallel(units_powers_invalid, units_powers_test, units_powers_valid)


def get_dict_from_dataframe(powers):
    for unit_index in range(powers.shape[1]):
        yield unit_index, powers[powers.iloc[0].index[unit_index]].tolist()


def _plot_powers_in_parallel(units_powers_invalid, units_powers_test, units_powers_valid):
    """
    Run the actual plotting on a process pool
    """
    unit_count = len(units_powers_valid.keys())
    print('Unit count:', unit_count)
    alpha = 0.1

    combination_index_combinations = [
        (combination_index, index_combination)
        for combination_index, index_combination
        in enumerate(combinations(range(unit_count), r=2))]
    function_partial = functools.partial(
        _plot_powers,
        alpha=alpha,
        units_powers_invalid=units_powers_invalid,
        units_powers_valid=units_powers_valid,
        units_powers_test=units_powers_test)
    multiprocessing.Pool(int(multiprocessing.cpu_count() / 2)) \
        .map(function_partial, combination_index_combinations)


def _plot_powers(combination_index_combination, alpha,
                 units_powers_invalid, units_powers_valid, units_powers_test):

    # unpack non-partial parameter
    combination_index = combination_index_combination[0]
    index_combination = combination_index_combination[1]
    del combination_index_combination

    print(f'Plotting combination #{combination_index} {index_combination}')

    figure = plt.figure()
    figure.set_size_inches(12.2, 12.2)  # TODO parametrise scaling!
    ax = figure.add_subplot(111)

    for color, validity, powers in zip(('crimson', 'blue', 'orange'),
                                       ('invalid', 'valid', 'test'),
                                       (units_powers_invalid, units_powers_valid, units_powers_test)):
        print(f'Plotting {validity} samples ...')

        # TODO
        # if validity != 'test':
        #    continue

        powers_x = powers[index_combination[0]]
        powers_y = powers[index_combination[1]]

        marker_size = (960) / len(powers)
        print('Marker size:', marker_size)

        alpha_now = alpha / 4 if validity == 'test' else alpha
        marker_size_now = marker_size * 2 if validity == 'test' else marker_size

        ax.scatter(powers_x, powers_y,
                   s=marker_size_now, c=color, alpha=alpha_now,
                   marker="o", edgecolor='None')

    plt.xlabel(s=f'unit {index_combination[0]}')
    plt.ylabel(s=f'unit {index_combination[1]}')
    plt.tight_layout(pad=0.0, w_pad=0.0, h_pad=0.0)
    plt.savefig(os.path.join(LAST_TRAINING_GRAPHS_PATH,
                             f'{combination_index}_{index_combination}.png'))
    plt.close(figure)
