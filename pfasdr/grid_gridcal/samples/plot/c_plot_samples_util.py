from pfasdr.neural.d_nn_util import load_valid_data_set_from_csv_files, load_data_set_from_ssv_files
from pfasdr.util.situations_persistence import COLUMN_RANGE_INPUT, COLUMN_RANGE_OUTPUT


def _get_powers_from_situations():
    data_set = load_valid_data_set_from_csv_files(use_columns=COLUMN_RANGE_INPUT)
    return data_set


def _get_validities_from_situations():
    data_set = load_valid_data_set_from_csv_files(use_columns=COLUMN_RANGE_OUTPUT)
    return data_set


def get_test_powers_from_fake_data(limit=-1):
    # TODO make path dynamic
    file_path = '/home/bengt/Promotion/pfasdr/experiments/DynSim/trainings/sampling.2018-03-01/training.2018-03-12.13_44_49/data_eval.txt'
    data_set = load_data_set_from_ssv_files(file_path, limit=limit)
    return data_set


if __name__ == '__main__':
    powers_valid = get_valid_powers_from_situations(limit=100)
    print('Valid powers:', len(powers_valid))

    powers_invalid = get_invalid_powers_from_situations(limit=100)
    print('Invalid powers:', len(powers_invalid))
