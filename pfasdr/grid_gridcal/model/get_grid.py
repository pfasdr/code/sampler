from pfasdr.grid_gridcal.model.get_european_grid import get_european_grid


def get_grid(
    *,
    topology_adding_function,
    topology_adding_function_parameter
):
    grid = get_european_grid()

    topology_adding_function(
        bus_number=topology_adding_function_parameter,
        grid=grid,
    )

    grid.compile()

    return grid
