from GridCal.Engine.Devices.branch import Branch
from GridCal.Engine.Devices.bus import Bus
from GridCal.Engine.Devices.load import Load


def add_slack_line_topology_to_grid(grid, bus_number):
    if bus_number < 1:
        raise ValueError('Need at least one bus')

    buses = []
    # Add one slack bus
    bus_0 = Bus('Bus 1', vnom=20, is_slack=True)
    grid.add_bus(bus_0)
    buses.append(bus_0)

    # Add non-slack buses
    for i in range(bus_number):
        bus = Bus('Bus 1', vnom=20)
        grid.add_bus(bus)
        grid.add_load(bus, Load('load 2', P=40, Q=20))
        buses.append(bus)

    # Add branches
    for i in range(bus_number):
        branch = Branch(
            buses[i],
            buses[i + 1],
            f'line {i}-{i + 1}',
            r=0.05,
            x=0.10,
            b=0.02,
        )
        grid.add_branch(obj=branch)
