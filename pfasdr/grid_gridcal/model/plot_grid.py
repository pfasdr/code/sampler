from matplotlib import pyplot as plt

from pfasdr.util.python_os.ensure_path_module import ensure_path
from pfasdr.util.paths import ROOT_PATH


def plot_grid(grid):
    grid.plot_graph()
    ensure_path(ROOT_PATH / 'data' / 'test')
    ensure_path(ROOT_PATH / 'data' / 'test' / 'output')
    plt.savefig(ROOT_PATH / 'data' / 'test' / 'output' / 'test_5_node.png')
