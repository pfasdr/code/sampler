from GridCal.Engine.Core.multi_circuit import MultiCircuit


def get_european_grid():
    """
    Create a multi circuit object with european grid parameters.

    :return: A multi circuit object with european grid parameters.
    """
    grid = MultiCircuit()

    grid.Sbase = 220
    grid.fBase = 50

    return grid
