import pprint


def print_grid(grid):
    print(f'\tBus Count: {len(grid.buses)}')
    print(f'\tBranch Count: {len(grid.branches)}')
    pprint.pprint(grid.__dict__)
