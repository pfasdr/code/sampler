import csv
import random
import datetime
import os
from ast import literal_eval

from GridCal.Engine.CalculationEngine import PowerFlow
from GridCal.Engine.CalculationEngine import Load
#from pfasdr.experiments.DynSim.dgs_parser import dgs_to_circuit
from GridCal.Engine.Importers.DGS_Parser import dgs_to_circuit

from share.python_sys.no_std_out import no_std_out
from pfasdr.util.paths import LAST_SITUATIONS_PATH, BUSES_15_JORGE_DGS_FILE_PATH
from pfasdr.util.situations_persistence import FIELDNAMES
from grid.power_flow.get_controllable_bus_attachments import get_controllable_bus_attachments
from grid.power_flow.check_validity import check_validity
from grid.power_flow.get_power_flow_options import get_power_flow_options
from share.python_os.ensure_file import ensure_file
from share.python_os.ensure_path import ensure_path


def import_circuit_from_dgs():
    """
    Opens Jorge's benchmark grid and parses its contents to a circuit.

    :return: The parsed circuit object
    """
    pass
    # TODO


class UnexpectedBusAttachmentError(Exception):
    pass


def evaluate_validity(circuit, powers, taps):
    # There are no transformer branches with a non-1 tap shift angle:
    #print(circuit.branches)
    #for branch in circuit.branches:
    #    assert branch.get_tap() == complex(1+0j)

    # Sanitize 0 to small values as per default value in GridCal
#    for branch in circuit.branches:
#        branch.G = 1e-20 if branch.G == 0 else branch.G
#        branch.B = 1e-20 if branch.B == 0 else branch.B

    # There are 7 loads, which can be controlled:
    bus_attachments = get_controllable_bus_attachments(circuit)
    for bus_attachment, power in zip(bus_attachments, powers):
        if type(bus_attachment) is Load:
            # 80 % active, 20 % reactive power
            bus_attachment.S = complex(0.8 * power, 0.2j * power)
            continue
        print(type(bus_attachment))
        raise UnexpectedBusAttachmentError()

    #taps = list()
    #taps.append(1.0 + 0.01 * -3)  # neutral position for tap changing transformer 0 (-10 ... +10), 1% change per step
    #taps.append(1.0 + -0.015 * +11)  # neutral position for tap changing transformer 13 (1 ... 27), -1.5 % change per step
    transformers = [branch for branch in circuit.branches if branch.is_transformer]
    for tap, branch in zip(taps, transformers):
        #print(branch.name)
        if branch.is_transformer:
            branch.tap_module = tap
            continue
        raise Exception()
        #if branch.name == 'Line Bus2-Bus4':
        #    branch.active = False

    # After changing the load powers, the circuit has to be compiled:
    #print('Power Flow Input:', circuit.power_flow_input)  # None
    circuit.compile()
    #print('Power Flow Input:', circuit.power_flow_input)  # PowerFlowInput

    # Only after compilation, the circuit can be plotted:
    #plot_circuit(circuit=circuit)

    # TODO Note that there is a nan and +inf at (5, 5)
    #print_bus_admittance_matrix(circuit=circuit)

    power_flow_options = get_power_flow_options()

    power_flow = PowerFlow(circuit, power_flow_options)

    #print('Power Flow Results:', circuit.power_flow_results)
    power_flow.run()
    #print('Power Flow Results:', circuit.power_flow_results)

    #print_power_flow_results_by_circuit(circuit=circuit)

    validity = check_validity(circuit.power_flow_input, circuit.power_flow_results)

    # The parsed circuit can already be printed:
    #print_circuit(circuit=circuit)

    #for branch in circuit.branches:
    #    print('Branch R:', branch.R)
    #    print('Branch X:', branch.X)
    #    print('Branch G:', branch.G)
    #    print('Branch B:', branch.B)

    #for bus in circuit.buses:
    #    print(bus)

    #print('Factor:', circuit.power_flow_input.Ybus.todense())
    #print('Divisor:', circuit.Sbase)
    #print('Division:', (circuit.power_flow_input.Ybus / circuit.Sbase).todense())

    return validity


def sample_generator():
    with no_std_out():
        circuit = dgs_to_circuit(filename=BUSES_15_JORGE_DGS_FILE_PATH)
    # circuit = MultiCircuit(); circuit.load_file(filename=BUSES_15_JORGE_FILE_PATH)

    # use values from dgs as start vector
    #powers_from_dgs_file = [3.75, 2.75, 3.10, 2.55, 4.42, 1.44, 5.63]
    #powers_from_experience = [15.0, 15.0, 13.0, 20.0, 18.0, 15.0, 20.0]
    #powers_by_compromise = [power * 0.6 for power in powers_from_experience]
    #print(powers_by_compromise)
    #TODO                  vvv
    #                     [9.0, 9.0, 7.8, 12.0, 10.8, 9.0, 12.0]
    # based on 2018-02-02:
    powers_as_necessary = [150., 10., 10, 14.0, 11.0, 13., 13.0]

    # Draw some samples and check their validity
    while True:
        powers_randomized = [power * random.random()
                             for power in powers_as_necessary]

        # from DGS file
        # taps = [random.choice(range(-10, 10)),
        #         random.choice(range(1, 27))]
        # Only combinations known to yield valid results
        # taps = [1, 1]
        taps = [0, 1]

        validity = evaluate_validity(circuit=circuit, powers=powers_randomized, taps=taps)

        sample = {
            'Validity': str(validity),
            'Tap0': str(taps[0]),
            'Tap1': str(taps[1]),
            'power0': powers_randomized[0],
            'power1': powers_randomized[1],
            'power2': powers_randomized[2],
            'power3': powers_randomized[3],
            'power4': powers_randomized[4],
            'power5': powers_randomized[5],
            'power6': powers_randomized[6],
        }
        yield sample

    # print(power_scaling_factor)


def write_samples_to_files(*, solutions_writer_invalid, solutions_writer_valid):
    sample_count_valid = 0
    sample_count_invalid = 0
    sample_count_skipped_invalid = 0
    sample_count_skipped_valid = 0
    for sample_count, sample in enumerate(sample_generator()):
        if literal_eval(sample['Validity']):
            if sample_count_invalid >= sample_count_valid:
                solutions_writer_valid.writerow(sample)
                sample_count_valid += 1
            else:
                sample_count_skipped_valid += 1
        else:
            if sample_count_valid >= sample_count_invalid:
                solutions_writer_invalid.writerow(sample)
                sample_count_invalid += 1
            else:
                sample_count_skipped_invalid += 1

        # Log yield rate to console
        if sample_count_invalid == sample_count_valid:
            sample_yield = (sample_count_invalid + sample_count_valid + 1) / (sample_count + 1)
            sample_yield = int(sample_yield * 100)
            sample_yield = str(sample_yield).rjust(2)
            print(f'Yield: {sample_yield} %, ' +
                  f'Total: {sample_count}, ' +
                  f'Invalid Skipped: {sample_count_skipped_invalid}, ' +
                  f'Valid Skipped: {sample_count_skipped_valid}, ' +
                  f'Invalid: {sample_count_invalid}, ' +
                  f'Valid: {sample_count_valid}')


def sample_to_file(index):
    ensure_path(LAST_SITUATIONS_PATH)
    date_path = os.path.join(LAST_SITUATIONS_PATH,
                             datetime.datetime.now().date().isoformat())
    ensure_path(date_path)

    file_name_valid = f'{index}_valid.csv'
    file_name_invalid = f'{index}_invalid.csv'

    situations_file_path_valid = os.path.join(date_path, file_name_valid)
    situations_file_path_invalid = os.path.join(date_path, file_name_invalid)

    ensure_file(situations_file_path_valid)
    ensure_file(situations_file_path_invalid)

    with open(situations_file_path_valid, 'w', newline='') as solutions_file_valid:
        with open(situations_file_path_invalid, 'w', newline='') as solutions_file_invalid:

            solutions_writer_valid = csv.DictWriter(solutions_file_valid, fieldnames=FIELDNAMES)
            solutions_writer_invalid = csv.DictWriter(solutions_file_invalid, fieldnames=FIELDNAMES)

            solutions_writer_valid.writeheader()
            solutions_writer_invalid.writeheader()

            # dive down to file-agnostic level here:
            write_samples_to_files(solutions_writer_invalid=solutions_writer_invalid,
                                   solutions_writer_valid=solutions_writer_valid)


if __name__ == "__main__":
    import multiprocessing

    pool = multiprocessing.Pool()
    pool.map(sample_to_file, range(os.cpu_count()))
