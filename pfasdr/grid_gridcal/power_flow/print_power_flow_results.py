import numpy as np
from numpy.core.multiarray import where


def print_power_flow_results(*,
                             power_flow_results):
    """
    @param power_flow_results: result of power flow calculation
    """

    np.core.arrayprint.set_printoptions(precision=3)

    print('\tVoltage:', abs(power_flow_results.voltage))
    print('\tSbranch:\t', abs(power_flow_results.Sbranch))
    print('\tLoading:\t', abs(power_flow_results.loading))  # * 100)
    print('\tError:\t\t', power_flow_results.error[0])
    print('\tConverged:\t', power_flow_results.converged[0])
    # print('\tViolations:\t',
    #       power_flow_results.check_limits(inputs=power_flow_input))
    # print('\tValidity:\t',
    #       check_validity(power_flow_input, power_flow_results))
    print()

    print('# Power Flow Results')
    print('\tPower flow converged:',
          power_flow_results.converged)
    print('\tRemaining Error:',
          power_flow_results.error)
    print('\tElapsed Time: ',
          power_flow_results.elapsed)
    print('\tMethod Calls:',
          power_flow_results.methods)
    print('\tInner Iterations:',
          power_flow_results.inner_iterations)
    print('\tOuter Iterations:',
          power_flow_results.outer_iterations)
    print()

    print('Loadings', power_flow_results.loading)

    overload_indices = list(where(power_flow_results.loading > 1)[0])
    print('Overloads at', overload_indices)
    overloads = power_flow_results.loading[overload_indices]
    print(len(overloads), 'Overloads:')
    print(overloads)

    voltages = abs(power_flow_results.voltage)
    print('Voltages', voltages)

    voltage_pu_max = 1.10
    voltage_pu_min = 0.90

    # deviation_summation = power_flow_results.check_limits(
    #     F=circuit.F,
    #     T=circuit.T,
    # )
    # print(deviation_summation)

    # branches:
    # Returns the loading rate when greater than 1 (nominal), zero otherwise
    overload_index = np.where(power_flow_results.loading > 1)[0]
    # bb_f = F[overload_index]
    # bb_t = T[overload_index]
    power_flow_results.overloads = power_flow_results.loading[overload_index]

    # Over and undervoltage values in the indices where it occurs
    voltages_abs = power_flow_results.voltage
    overvoltage_indices = np.where(voltages_abs > voltage_pu_max)[0]
    power_flow_results.overvoltage = \
        (voltages_abs - voltage_pu_max)[overvoltage_indices]
    undervoltage_indices = np.where(voltages_abs < voltage_pu_min)[0]
    power_flow_results.undervoltage = \
        (voltage_pu_min - voltages_abs)[undervoltage_indices]

    print('Overvoltage at', overvoltage_indices)
    overvoltages = (voltages - voltage_pu_max)[overvoltage_indices]
    print(len(overvoltages), 'Overvoltages:')
    print(overvoltages)

    under_voltage_indices = list(where(voltages < voltage_pu_min)[0])
    print('Undervoltage at', under_voltage_indices)
    undervoltages = (voltage_pu_min - voltages)[under_voltage_indices]
    print(len(undervoltages), 'Undervoltages:')
    print(undervoltages)

    print(
        'Violation sum:',
        abs(sum(overloads) + abs(sum(overvoltages)) + abs(sum(undervoltages)))
    )
