def check_validity(inputs, results):
    return results.check_limits(inputs=inputs) == 0
