from GridCal.Engine.Simulations.PowerFlow.power_flow_options import \
    PowerFlowOptions
from GridCal.Engine.Simulations.PowerFlow.solver_type import SolverType
from GridCal.Engine.basic_structures import BranchImpedanceMode


def get_power_flow_options():
    solver_type = SolverType.HELM
    verbose = True
    initialize_with_existing_solution = True
    dispatch_storage = False
    tolerance = 1e-3  # precision to which to iterate the calculation
    max_iterations = 100
    enforce_q_limits = False  # whether or not to control reactive power
    retry_with_other_methods = False
    max_outer_loop_iter = 100
    control_taps = False
    q_steepness_factor = 30
    branch_impedance_tolerance_mode = BranchImpedanceMode.Specified
    power_flow_options = PowerFlowOptions(
        solver_type=solver_type,
        retry_with_other_methods=retry_with_other_methods,
        verbose=verbose,
        initialize_with_existing_solution=initialize_with_existing_solution,
        dispatch_storage=dispatch_storage,
        tolerance=tolerance,
        max_iter=max_iterations,
        max_outer_loop_iter=max_outer_loop_iter,
        control_q=enforce_q_limits,
        control_taps=control_taps,
        multi_core=False,
        control_p=False,
        apply_temperature_correction=False,
        branch_impedance_tolerance_mode=branch_impedance_tolerance_mode,
        q_steepness_factor=q_steepness_factor,
    )

    return power_flow_options
