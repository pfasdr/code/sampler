from GridCal.Engine.Devices.load import Load

from pfasdr.grid_gridcal.power_flow.get_controllable_bus_attachments import \
    get_controllable_bus_attachments


def update_circuit(circuit, powers):
    attachments = get_controllable_bus_attachments(circuit=circuit)

    # update batteries at buses
    print(len(attachments))
    for index, power in zip(range(len(powers)), powers):
        if isinstance(attachments[index], Load):
            attachments[index].S = power

    circuit.compile()

    return circuit
