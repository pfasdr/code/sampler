from GridCal.Engine.Simulations.PowerFlow.power_flow_runnable import PowerFlow

from pfasdr.grid_gridcal.power_flow.get_power_flow_options import \
    get_power_flow_options


def calculate_power_flow_helm(circuit):
    """
    Execute a power flow calculation with the HELM algorithm.

    Convenience function which wraps the execution of a powerflow using HELM.

    See GridCal for details on how this works.

    :param circuit: The grid to be calculated
    :return: The calculated power flow
    """
    power_flow_options = get_power_flow_options()

    power_flow = PowerFlow(circuit, power_flow_options)

    return power_flow
