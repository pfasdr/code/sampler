from pfasdr.grid_gridcal.power_flow.print_power_flow_results import \
    print_power_flow_results


def print_power_flow_results_by_circuit(circuit):
    print_power_flow_results(
        power_flow_results=circuit.power_flow_results,
    )
