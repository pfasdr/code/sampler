from GridCal.Engine.Simulations.PowerFlow.power_flow_runnable import PowerFlow

from pfasdr.grid_gridcal.power_flow.exception import ConvergenceError
from pfasdr.grid_gridcal.power_flow.get_power_flow_options import \
    get_power_flow_options


def run_power_flow_on_grid(*,
                           grid):
    options = get_power_flow_options()

    power_flow = PowerFlow(
        grid=grid,
        options=options,
    )
    power_flow.run()
    results = power_flow.results

    if results.converged[0] is not True:
        raise ConvergenceError()

    return results
