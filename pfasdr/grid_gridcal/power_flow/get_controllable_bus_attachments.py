def get_controllable_bus_attachments(circuit):
    attachments = []

    for bus in circuit.buses:
        attachments += bus.loads
        attachments += bus.controlled_generators

        # There is nothing to be controlled about shunts
        # Well, actually .active, but I do not want to try that, now.
        # So skip them at least for now
        # attachments += bus.shunts

        attachments += bus.batteries

        # There is nothing to be controlled about static generators
        # Well, actually .active, but I do not want to try that, now.
        # So skip them at least for now
        # attachments += bus.static_generators

    return attachments
