from pfasdr.grid_gridcal.power_flow.calculate_power_flow_helm import \
    calculate_power_flow_helm
from pfasdr.grid_gridcal.power_flow.update_circuit import update_circuit
from pfasdr.grid_gridcal.power_flow.check_validity import check_validity


def evaluate_powers_on_circuit(circuit, powers):
    """
    Evaluate the given powers on the given circuit.
    """
    circuit = update_circuit(circuit, powers)

    # This is the computationally expensive call.
    calculate_power_flow_helm(circuit=circuit)

    validity = check_validity(inputs=circuit.power_flow_input,
                              results=circuit.power_flow_results)

    return validity
