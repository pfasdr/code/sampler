def monitor_training(
    *,
    skip: bool,
):
    if skip:
        print('Skipping training process monitoring.')
        return

    print('Monitoring training process ...')
