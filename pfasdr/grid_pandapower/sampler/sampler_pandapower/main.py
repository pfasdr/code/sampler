from pfasdr.grid_pandapower.sampler.sampler_pandapower.\
    evaluate_situation_pandapower_module import \
    evaluate_situation_pandapower
from pfasdr.grid_pandapower.sampler.sampler_pandapower.sample_panda_power import \
    sample_pandapower

if __name__ == '__main__':
    # Do not seed to ensure actually different samples each run.
    # random.seed(42)

    # (16, 17, 18, 19, 20) have errors, so lets only sample [1, 16)
    # load_counts_pandapower = tuple(range(1, 16))
    load_counts_pandapower = (100, )

    sample_pandapower(
        evaluate_situation_function=evaluate_situation_pandapower,
        load_counts=load_counts_pandapower,
    )
