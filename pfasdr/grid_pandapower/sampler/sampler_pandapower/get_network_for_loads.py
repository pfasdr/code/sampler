import pandapower

from pfasdr.grid_pandapower.sampler.sampler_pandapower.get_empty_network import \
    CachingEmptyNetworkBorg


def get_network_for_loads(pq_loads_mva):
    network = CachingEmptyNetworkBorg().empty_network

    # Create medium voltage grid
    bus_0 = pandapower.create_bus(
        net=network,
        vn_kv=20.,
        name="Bus 0",
    )
    pandapower.create_ext_grid(
        net=network,
        bus=bus_0,
        name="Grid Connection",
    )

    # Create transformer to low voltage grid
    bus_1 = pandapower.create_bus(
        net=network,
        vn_kv=0.4,
        name="Bus 1",
    )
    pandapower.create_transformer(
        net=network,
        hv_bus=bus_0,
        lv_bus=bus_1,
        std_type="0.4 MVA 20/0.4 kV",
        name="Trafo",
    )

    # Create low voltage lines and attach loads
    buses = [bus_0, bus_1]
    for index, pq_load in enumerate(pq_loads_mva):
        bus = pandapower.create_bus(
            net=network,
            vn_kv=0.4,
            name=f'Bus {index}',
        )
        # These carry up to 144 A when buried.
        pandapower.create_line(
            net=network,
            from_bus=buses[len(buses) - 1],
            to_bus=bus,
            length_km=0.1,
            name="Line",
            std_type="NAYY 4x50 SE",
        )
        pandapower.create_load(
            net=network,
            bus=bus,
            p_mw=pq_load.real,
            q_mvar=pq_load.imag,
            name=f'Load {index}',
        )

    return network
