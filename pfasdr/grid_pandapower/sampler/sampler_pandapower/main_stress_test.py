from random import seed

from pfasdr.grid_pandapower.sampler.sampler.sample import sample
from pfasdr.grid_pandapower.sampler.sampler_pandapower.\
    evaluate_situation_pandapower_module import \
    evaluate_situation_pandapower

if __name__ == '__main__':
    seed(42)
    for index in range(50):
        print(index)
        sample(
            load_count=17,
            evaluate_situation_function=evaluate_situation_pandapower,
        )
