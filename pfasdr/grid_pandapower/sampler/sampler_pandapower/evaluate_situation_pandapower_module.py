import csv

import pandapower

from pfasdr.grid_pandapower.sampler.sampler_pandapower.get_network_for_loads \
    import get_network_for_loads
from pfasdr.util.python_os.ensure_path_module import ensure_path
from pfasdr.util.paths import ROOT_PATH


# @pandapower.jit()# nopython=True)
def evaluate_situation_pandapower(pq_loads_mva):
    network = get_network_for_loads(pq_loads_mva)
    # Simplify powers from complex to P-only
    # This saves about 50 % of disk space
    pq_loads_mva = [
        load.real + load.imag
        for load in pq_loads_mva
    ]
    common_csv_files_path = \
        ROOT_PATH / 'data' / 'pandapower' / 'linear_grid' / \
        ('length_' + str(len(pq_loads_mva)))
    ensure_path(str(common_csv_files_path))
    try:
        pandapower.runpp(
            net=network,
            numba=True,
        )
    except pandapower.powerflow.LoadflowNotConverged:
        # This power flow did not converge.
        # TODO
        error_csv_file_path = common_csv_files_path / 'errors.csv'
        ensure_path(error_csv_file_path)
        with open(error_csv_file_path, 'a') as error_file:
            csv \
                .writer(error_file) \
                .writerow(
                pq_loads_mva,
            )
        return False
        # raise
    overloaded_lines = pandapower.overloaded_lines(
        net=network,
        max_load=100,
    )
    violated_buses = pandapower.violated_buses(
        net=network,
        min_vm_pu=0.9,
        max_vm_pu=1.1,
    )
    if len(overloaded_lines) > 0 or len(violated_buses) > 0:
        # We found an invalid bisecting factor.
        # So record and report that back.
        invalid_csv_file_path = common_csv_files_path / 'invalid.csv'
        ensure_path(invalid_csv_file_path)
        with open(invalid_csv_file_path, 'a') as error_file:
            csv \
                .writer(error_file) \
                .writerow(
                pq_loads_mva,
            )
        return False
    else:
        # We have no violations of grid restrictions
        # So record and report that back.
        valid_csv_file_path = common_csv_files_path / 'valid.csv'
        ensure_path(valid_csv_file_path)
        with open(valid_csv_file_path, 'a') as error_file:
            csv \
                .writer(error_file) \
                .writerow(
                    pq_loads_mva,
                )
        return True
