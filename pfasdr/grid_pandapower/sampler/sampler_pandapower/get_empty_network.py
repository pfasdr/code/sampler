from copy import deepcopy

import pandapower


class CachingEmptyNetworkBorg(object):
    __shared_state = {}

    _empty_network = None

    def __init__(self):
        self.__dict__ = self.__shared_state
        if self._empty_network is None:
            print('Creating empty network once.')
            self._empty_network = pandapower.create_empty_network()

    @property
    def empty_network(self):
        return deepcopy(self._empty_network)
