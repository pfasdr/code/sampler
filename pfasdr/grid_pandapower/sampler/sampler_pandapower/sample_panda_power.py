import concurrent
from concurrent.futures.process import ProcessPoolExecutor
import itertools
import multiprocessing
import random

from pfasdr.grid_pandapower.sampler.sampler.log_finished_sample_future import \
    log_finished_sample_future
from pfasdr.grid_pandapower.sampler.sampler.sample import sample


def sample_pandapower(
    evaluate_situation_function,
    load_counts,
    limit=-1,
):
    cpu_count = multiprocessing.cpu_count()
    executor = ProcessPoolExecutor(
        max_workers=cpu_count,
    )

    future_to_params = {}
    skip_counter = 0
    for power_distribution_index in itertools.count():
        if power_distribution_index == limit:
            return

        if len(future_to_params) > cpu_count:
            skip_counter += 1
        else:
            load_count = random.choice(load_counts)
            future = executor.submit(
                sample,
                load_count,
                evaluate_situation_function,
            )
            future_to_params[future] = (
                sample.__name__,
                load_count,
                power_distribution_index - skip_counter,
            )

        try:
            done, _ = concurrent.futures.wait(
                future_to_params,
                return_when=concurrent.futures.FIRST_COMPLETED,
                timeout=.1,
            )
        except KeyboardInterrupt:
            break
        if len(done) == 0:
            continue

        for future in done:
            params = future_to_params.pop(future)
            log_finished_sample_future(future=future, params=params)
