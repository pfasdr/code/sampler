def log_finished_sample_future(
        *,
        future,
        params,
):
    print(
        f'finished {params}, '
        f'value: {future.result()}, '
        f'delta: {1 - future.result()[0] / future.result()[1]}'
    )
