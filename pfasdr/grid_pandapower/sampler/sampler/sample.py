import random
from functools import partial

from pfasdr.grid_pandapower.sampler.explore_and_exploit.explore_and_exploit \
    import explore_and_exploit
from pfasdr.grid_pandapower.sampler.sampler.evaluate_power_scaling_factor \
    import evaluate_power_scaling_factor


def sample(
    load_count,
    evaluate_situation_function,
):
    power_factor = complex(0.95, 0.05)
    phase_count = 3
    voltage_v = 400
    amperage_a = 63
    va_to_mva = 1 / 1_000_000
    pq_loads_mva = [
        random.random() *
        power_factor * phase_count * voltage_v * amperage_a * va_to_mva
        for _ in range(load_count)
    ]

    # The low voltage grid can only get external power via its transformer.
    # The transformer has a limit in its loading of 400 kVA.
    # The loading sum of the low voltage grid may not be bigger than that.
    # Bigger means here production and consumption.
    # So create list of loads adhering  to this principle
    load_sum_mva = sum(
        # sqrt(load_mva.real ** 2 + load_mva.imag ** 2)
        load_mva
        for load_mva in pq_loads_mva
    )
    loading_max_transformer_mva = 0.4
    transformer_overloading_factor = (
                load_sum_mva / loading_max_transformer_mva).real

    # TODO Automate this
    # transformer_overloading_factor *= 141 / 521  # empirical value for 20 nodes
    # transformer_overloading_factor *= 304 / 1300  # empirical value for 50 nodes
    transformer_overloading_factor *= 917 / 7500 / 10  # empirical value for 100 nodes

    lower, upper = explore_and_exploit(
        small_enough_function=partial(
            evaluate_power_scaling_factor,
            pq_loads_mva=pq_loads_mva,
            evaluate_situation_function=evaluate_situation_function,
        ),
        start_value=transformer_overloading_factor,
    )

    return lower, upper
