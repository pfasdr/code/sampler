

def evaluate_power_scaling_factor(
    *,
    candidate_value, pq_loads_mva, evaluate_situation_function,
):
    # TODO Move this to another wrapper function
    bisecting_factor_candidate = candidate_value

    # Scale loads
    pq_loads_mva = [
        load * bisecting_factor_candidate
        for load in pq_loads_mva
    ]

    return evaluate_situation_function(pq_loads_mva)
