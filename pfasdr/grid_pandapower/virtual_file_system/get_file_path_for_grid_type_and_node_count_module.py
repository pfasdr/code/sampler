from pathlib import Path

from pfasdr.util.paths import ROOT_PATH

LINEAR_PROJECT_PATH = \
    'pfasdr.data.grid.samples/pfasdr.data.grid.samples.linear-1-15-node'


def get_file_path_for_grid_type_and_node_count_and_validity(
    *,
    grid_type: str = 'linear',
    node_count: int = 3,
    validity: str = 'invalid',
):
    if grid_type == 'linear':
        if 0 < node_count < 20:

            project_path = \
                LINEAR_PROJECT_PATH
            project_relative_path = \
                Path('data') / f'length_{node_count}' / f'{validity}.csv'
            filepath_valid: Path = \
                ROOT_PATH / 'data' / project_path / project_relative_path

            return filepath_valid

        if node_count >= 20:
            raise NotImplementedError()
