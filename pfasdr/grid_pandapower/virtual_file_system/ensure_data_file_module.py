from pathlib import Path

from pfasdr.grid_pandapower.virtual_file_system.download_data_file_module \
    import download_data_file
from pfasdr.grid_pandapower.plotter.project_paths_to_project_identiifers \
    import PROJECT_PATHS_TO_PROJECT_IDENTIFIERS
from pfasdr.util.paths import ROOT_PATH


def ensure_data_file(
    *,
    filepath: Path,
):
    try:
        open(file=str(filepath))
    except FileNotFoundError:
        print('Downloading missing data input_file:', filepath)

        # Derive values from input
        project_path = str(filepath).split(str(ROOT_PATH / 'data'))[1]
        project_path = project_path.split('data/')[0]
        project_path = project_path.lstrip('/').rstrip('/')

        project_identifier = PROJECT_PATHS_TO_PROJECT_IDENTIFIERS[project_path]

        project_relative_path = str(filepath).split(project_path)[1].lstrip('/')

        download_data_file(
            filepath=filepath,
            project_identifier=project_identifier,
            project_path=project_path,
            project_relative_path=project_relative_path,
        )
