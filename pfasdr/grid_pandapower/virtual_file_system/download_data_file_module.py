import gitlab as python_gitlab

from pfasdr.grid_pandapower.virtual_file_system.known_project_paths_module \
    import KNOWN_PROJECT_PATHS
from pfasdr.util.python_os.ensure_path_module import ensure_path


def download_data_file(filepath, project_identifier, project_path,
                       project_relative_path):
    if project_path not in KNOWN_PROJECT_PATHS:
        raise NotImplementedError(
            'Unable to download data like this:', filepath
        )

    # private_access_token = '2ihQQ-JhotB-svi7rVf2'
    # private token or personal token authentication
    gitlab = python_gitlab.Gitlab(
        'https://gitlab.com',
        #    private_token=private_access_token,
        api_version='4',
    )
    # gitlab.auth()

    project = gitlab.projects.get(project_identifier)
    input_file = project.files.get(
        file_path=str(project_relative_path),
        ref='master',
    )

    ensure_path(filepath)
    with open(filepath, 'wb') as output_file:
        output_file.write(input_file.decode())
