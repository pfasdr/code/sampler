import pandas as pd

from pfasdr.grid_pandapower.virtual_file_system.ensure_data_file_module import \
    ensure_data_file
from pfasdr.grid_pandapower.plotter.get_name_list_from_node_count_module \
    import get_name_list_from_node_count
from pfasdr.grid_pandapower.virtual_file_system.\
    get_file_path_for_grid_type_and_node_count_module import \
    get_file_path_for_grid_type_and_node_count_and_validity


def load_data_frames(node_count):
    names = get_name_list_from_node_count(node_count=node_count)

    filepath_valid = \
        get_file_path_for_grid_type_and_node_count_and_validity(
            grid_type='linear',
            node_count=node_count,
            validity='valid'
        )
    ensure_data_file(filepath=filepath_valid)
    data_frame_valid = pd.read_csv(
        filepath_or_buffer=filepath_valid,
        names=names,
    )

    filepath_invalid = \
        get_file_path_for_grid_type_and_node_count_and_validity(
            grid_type='linear',
            node_count=node_count,
            validity='invalid'
        )
    ensure_data_file(filepath=filepath_invalid)
    data_frame_invalid = pd.read_csv(
        filepath_or_buffer=filepath_invalid,
        names=names,
    )

    return data_frame_invalid, data_frame_valid
