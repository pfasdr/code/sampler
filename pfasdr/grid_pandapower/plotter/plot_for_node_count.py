from pfasdr.grid_pandapower.virtual_file_system.load_data_frames import \
    load_data_frames
from pfasdr.grid_pandapower.plotter.plot_scatter_2d_matrix_module import \
    plot_scatter_2d_matrix
from pfasdr.grid_pandapower.plotter.plot_scatter_2d_single_module import \
    plot_scatter_2d_single
from pfasdr.grid_pandapower.plotter.plot_scatter_3d_single_module import \
    plot_scatter_3d_single
from pfasdr.grid_pandapower.plotter.plot_voronoi_2d_single_module import \
    plot_voronoi_2d_single


def plot_for_node_count(
    *,
    node_count
):
    df_invalid, df_valid = load_data_frames(node_count=node_count)

    plot_scatter_2d_matrix(
        df_invalid=df_invalid,
        df_valid=df_valid,
        node_count=node_count,
        start=0,
        end=1000,
    )

    for x in range(node_count):
        for y in range(node_count):
            if x == y:
                continue

            plot_scatter_2d_single(
                df_invalid=df_invalid,
                df_valid=df_valid,
                start=0,
                end=1000,
                x=x,
                y=y,
            )
            plot_voronoi_2d_single(
                df_invalid=df_invalid,
                df_valid=df_valid,
                x=x,
                y=y,
                start=0,
                end=100,
            )

            for z in range(node_count):
                if z == y:
                    continue
                if z == x:
                    continue

                plot_scatter_3d_single(
                    df_invalid=df_invalid,
                    df_valid=df_valid,
                    start=0,
                    end=10000,
                    x=x,
                    y=y,
                    z=z,
                )
