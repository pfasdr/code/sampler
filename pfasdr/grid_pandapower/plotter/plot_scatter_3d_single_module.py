from matplotlib import pyplot


def plot_scatter_3d_single(
        *,
        df_invalid, df_valid,
        x, y, z, start=0, end,
):
    from mpl_toolkits.mplot3d import Axes3D  # noqa: F401 unused import

    subplot = pyplot.figure().gca(projection='3d')
    subplot.scatter(
        xs=df_valid[start:end][[x]],
        ys=df_valid[start:end][[y]],
        zs=df_valid[start:end][[z]],
        color='g',
        s=1,
    )
    subplot.scatter(
        xs=df_invalid[start:end][[x]],
        ys=df_invalid[start:end][[y]],
        zs=df_invalid[start:end][[z]],
        color='r',
        s=1,
    )
    pyplot.show()
