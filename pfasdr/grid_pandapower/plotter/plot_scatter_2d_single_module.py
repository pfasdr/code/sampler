from matplotlib import pyplot


def plot_scatter_2d_single(
    *,
    df_invalid, df_valid,
    x, y, start=0, end,
):
    axes = df_valid[start:end].plot(
        x=x,
        y=y,
        kind='scatter',
        color='g',
    )
    df_invalid[start:end].plot(
        ax=axes,
        x=x,
        y=y,
        kind='scatter',
        color='r',
    )
    pyplot.show()
