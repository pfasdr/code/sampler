from pfasdr.grid_pandapower.plotter.plot_for_node_count import \
    plot_for_node_count


def main():
    plot_for_node_count(node_count=3)


if __name__ == '__main__':
    main()
