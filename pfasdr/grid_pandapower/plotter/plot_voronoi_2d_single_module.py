from matplotlib import pyplot as plt
from scipy.spatial import voronoi_plot_2d
from scipy.spatial.qhull import Voronoi


def plot_voronoi_2d_single(
    *,
    df_invalid, df_valid, x, y, start, end
):
    voronoi = Voronoi(
        list(
            zip(
                df_valid[start:end][x] + df_invalid[start:end][x],
                df_valid[start:end][y] + df_invalid[start:end][y]
            )
        )
    )
    voronoi_plot_2d(voronoi)
    plt.show()
