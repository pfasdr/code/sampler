from matplotlib import pyplot
from matplotlib.axes import Axes

from pfasdr.grid_pandapower.plotter.whiten_axes import whiten_axes


def plot_scatter_2d_matrix(
    *,
    df_invalid, df_valid, node_count,
    start=0, end,
):
    _, axes = pyplot.subplots(
        nrows=node_count - 1,
        ncols=node_count - 1,
        sharey='all',
        sharex='all',
    )

    for x in range(1, node_count):
        for y in range(node_count - 1):
            axes_current: Axes = axes[x - 1][y]
            if x <= y:
                whiten_axes(axes=axes_current)
                # axes_current.set_xlabel(str(x))
                # axes_current.set_ylabel(str(y))
                continue

            print('x:', x)
            print('y:', y)

            axes_current = df_invalid[start:end].plot(
                ax=axes_current,
                color='r',
                kind="scatter",
                s=1,
                x=x,
                y=y,
            )
            df_valid[start:end].plot(
                ax=axes_current,
                color='g',
                s=1,
                kind="scatter",
                x=x,
                y=y,
            )

    pyplot.show()
    return
