import pandapower


def determine_validity(
    *,
    net: pandapower.pandapowerNet
) -> bool:
    overloaded_lines = pandapower.overloaded_lines(
        net=net,
        max_load=100,
    )
    violated_buses = pandapower.violated_buses(
        net=net,
        min_vm_pu=0.9,
        max_vm_pu=1.1,
    )

    # if overloaded_lines.size > 0:
    #     print(
    #         f'Grid state invalid. ({overloaded_lines.size} overloaded lines)'
    #     )
    # if violated_buses.size > 0:
    #     print(
    #         f'Grid state invalid. ({violated_buses.size} violated buses)'
    #     )

    if overloaded_lines.size > 0:
        return False
    if violated_buses.size > 0:
        return False

    # print('Grid state valid.')
    return True
