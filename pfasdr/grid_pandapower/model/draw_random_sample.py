from pandapower import pandapowerNet
import numpy

from pfasdr.grid_pandapower.model.draw_sample_parametric import \
    draw_sample_parametric


def draw_sample_random(net=pandapowerNet):
    load_scaling = numpy.random.uniform(0, 1, size=(net.load.shape[0], 1))
    sgen_scaling = numpy.random.uniform(0, 1, size=(net.sgen.shape[0], 1))

    return draw_sample_parametric(
        net=net,
        load_scaling=load_scaling,
        sgen_scaling=sgen_scaling,
    )
