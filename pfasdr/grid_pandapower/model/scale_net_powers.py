from pandapower import pandapowerNet


def scale_net_powers(
    net: pandapowerNet,
    load_scaling,
    sgen_scaling,
) -> pandapowerNet:
    # Check input sanity
    if load_scaling.shape[0] != net.load.shape[0]:
        raise ValueError()
    if sgen_scaling.shape[0] != net.sgen.shape[0]:
        raise ValueError()

    # Scale loads and generators
    net.load.scaling = load_scaling
    net.sgen.scaling = sgen_scaling

    return net
