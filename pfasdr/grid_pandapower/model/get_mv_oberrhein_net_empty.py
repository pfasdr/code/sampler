import os
from functools import lru_cache

import numpy
import pandapower


def _load_mv_oberrhein_net():
    net: pandapower.pandapowerNet = pandapower.from_json(
        os.path.join(pandapower.pp_dir, "networks", "mv_oberrhein.json")
    )

    return net


@lru_cache(1)
def get_mv_oberrhein_net_empty():
    net: pandapower.pandapowerNet = _load_mv_oberrhein_net()

    # Set unit parameters
    cosphi_load: float = 0.98  # AC/DC Loads, "from experience"
    cosphi_sgen: float = 1.0   # Pure PV Generators, "from experience"

    # Calculate power factors
    net.load.q_mvar = numpy.tan(numpy.arccos(cosphi_load)) * net.load.p_mw
    net.sgen.q_mvar = numpy.tan(numpy.arccos(cosphi_sgen)) * net.sgen.p_mw

    # Set tap changing transformers to the "generation" scenario
    transformers_with_taps = net.trafo[net.trafo.sn_mva > 1].index
    net.trafo.tap_pos.loc[transformers_with_taps] = [0, 0]

    # Run a power flow calculation on the empty network.
    # This seems to help performance of subsequent power flow calculations.
    pandapower.runpp(
        net=net,
        algorithm='nr',
        max_iteration=10,
        tolerance_mva=1e-8,
        trafo_loading='current',
        enforce_q_lims=False,
        check_connectivity=True,
        voltage_depend_loads=True,
        numba=True,
        switch_rx_ratio=2.0,
        v_debug=False,
        init_va_degree=None,
        recycle=None,
        neglect_open_switch_branches=False,
    )

    return net
