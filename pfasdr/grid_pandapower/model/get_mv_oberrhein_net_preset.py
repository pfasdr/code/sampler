from pandapower import pandapowerNet
from pandapower.networks import mv_oberrhein


def get_mv_oberrhein_net_preset():
    """
    Get the MV Oberrhein network.

    This loads the MV Oberrhein net from file, sets the power factor,
    scales its powers and runs a power flow.

    :return:
    """
    scenario: str = 'load'
    cosphi_load: float = 0.98
    cosphi_pv: float = 1.0
    include_substations: bool = False
    separation_by_sub: bool = False

    net: pandapowerNet = mv_oberrhein(
        scenario=scenario,
        cosphi_load=cosphi_load,
        cosphi_pv=cosphi_pv,
        include_substations=include_substations,
        separation_by_sub=separation_by_sub,
    )

    return net
