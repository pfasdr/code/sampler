import pandas


def configure_pandas_printing():
    pandas.set_option('display.max_columns', 500)
    pandas.set_option('display.max_rows', 20)
    pandas.set_option('display.width', 1000)
