from pandapower import runpp, pandapowerNet

from pfasdr.grid_pandapower.model.determine_validity_module import \
    determine_validity
from pfasdr.grid_pandapower.model.scale_net_powers import scale_net_powers


def draw_sample_parametric(
    *,
        net: pandapowerNet,
        load_scaling,
        sgen_scaling,
) -> object:
    net = scale_net_powers(
        net=net,
        load_scaling=load_scaling,
        sgen_scaling=sgen_scaling,
    )
    runpp(net)

    validity: bool = determine_validity(net=net)

    return validity
