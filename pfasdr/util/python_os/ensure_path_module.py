import os


def ensure_path(path):
    """
    Make sure the given path exists.

    Create parent paths, if necessary.
    Like $ mkdir -p
    """
    path = str(path)
    path = os.path.dirname(path)
    if not path.endswith(os.sep):
        path += os.sep

    try:
        os.listdir(path)
    except (IOError, OSError):
        try:
            # path was not present, but parent
            os.mkdir(path)
        except FileNotFoundError:
            # neither path nor parent were present
            # so recurse with parent path
            if path.startswith(os.sep):
                # absolute path
                parent = os.path.join('/', *os.path.split(path)[:-1])
            else:
                # relative path
                parent = os.path.join(*os.path.split(path)[:-1])

            ensure_path(parent)
            ensure_path(path)
        except FileExistsError:
            # the directory was already there
            # so skip creation an be done with it
            pass
