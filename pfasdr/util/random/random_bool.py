from random import random


def rand_bool():
    """
    Generate a random boolean value.
    """
    # This might just be the quickest way to get a random boolean
    return random() < 0.5
