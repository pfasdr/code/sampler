sed -i "/zipp==/c\zipp==3.1.0" requirements.d/venv.txt
python -m pur --nonzero-exit-code -r requirements.d/venv.txt
rc=$?
echo $rc
if [ $rc -eq 11 ]
  then
      echo $rc
      sed -i "/zipp==/c\zipp==1.2.0" requirements.d/venv.txt
      cat requirements.d/venv.txt
      git commit -m 'Update requirements - done by pur' requirements.d/venv.txt
      git push
      exit 0
    else exit 0
fi
