from GridCal.Engine.Core.multi_circuit import MultiCircuit
from GridCal.Engine.Core.numerical_circuit import NumericalCircuit
from GridCal.Engine.Simulations.PowerFlow.power_flow_results import \
    PowerFlowResults

from pfasdr.grid_gridcal.model.plot_grid import plot_grid
from pfasdr.grid_gridcal.model.print_grid import print_grid
from pfasdr.grid_gridcal.power_flow.print_power_flow_results import \
    print_power_flow_results


def test_5_node_type(grid):
    assert isinstance(grid, MultiCircuit)


def test_5_node_is_compiled(grid):
    assert grid.numerical_circuit is not None
    assert isinstance(grid.numerical_circuit, NumericalCircuit)


def test_5_node_plot(grid):
    plot_grid(grid)


def test_5_node_print(grid):
    print_grid(grid)


def test_5_node_power_flow_run(power_flow_results):
    assert isinstance(power_flow_results, PowerFlowResults)

    # TODO Why only 0th convergence?
    # TODO Maybe because of the profiles feature?
    assert power_flow_results.converged[0]


def test_5_node_power_flow_print(*,
                                 # power_flow_input,
                                 power_flow_results):
    print_power_flow_results(
        # power_flow_input=power_flow_input,
        power_flow_results=power_flow_results,
    )
