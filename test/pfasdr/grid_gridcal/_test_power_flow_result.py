from GridCal.Engine.Simulations.PowerFlow.power_flow_results import PowerFlowResults


def test_power_flow_results_type(power_flow_results):
    assert power_flow_results is not None
    assert isinstance(power_flow_results, PowerFlowResults)
