import pytest

from GridCal.Engine.Core.multi_circuit import MultiCircuit

from pfasdr.grid_gridcal.model.get_grid import get_grid
from pfasdr.grid_gridcal.model.add_slack_line_topology_to_grid_ import \
    add_slack_line_topology_to_grid
from pfasdr.grid_gridcal.power_flow.run_power_flow import run_power_flow_on_grid


@pytest.fixture(
    params=[1, 2, 3, 4],
)
def _grid(request):
    grid: MultiCircuit = get_grid(
        topology_adding_function=add_slack_line_topology_to_grid,
        topology_adding_function_parameter=request.param,
    )

    return grid


@pytest.fixture
def _power_flow_results(grid):
    power_flow_results:object = run_power_flow_on_grid(grid=grid)

    return power_flow_results
