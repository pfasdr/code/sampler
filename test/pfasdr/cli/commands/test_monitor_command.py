from pfasdr.cli.commands.monitor import monitor_training


def test_test_command():
    # Mock
    exit = True

    # Test
    monitor_training(
        skip=exit,
    )

    # Assert
    assert True
