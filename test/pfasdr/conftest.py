def pytest_configure(config):
    config.addinivalue_line(
        "markers", "stage(name): mark test to run only on named environment"
    )


def pytest_addoption(parser):
    parser.addoption(
        "--stage",
        action="store",
        metavar="NAME",
        help="only run tests matching the stage NAME.",
    )
