from pfasdr.grid_pandapower.sampler.explore_and_exploit.\
    explore_and_exploit import explore_and_exploit
from pfasdr.grid_pandapower.sampler.explore_and_exploit.\
    is_smaller_than_seven import is_small_enough


def test_is_small_enough_seven():
    start_value = 3

    explore_and_exploit(
        small_enough_function=is_small_enough,
        start_value=start_value,
    )
