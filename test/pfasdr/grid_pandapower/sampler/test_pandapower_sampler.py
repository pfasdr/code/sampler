from pfasdr.grid_pandapower.sampler.sampler.sample import sample
from pfasdr.grid_pandapower.sampler.sampler_pandapower.\
    evaluate_situation_pandapower_module import evaluate_situation_pandapower


def test_pandapower_sampler():
    load_count = 2
    sample(
        load_count=load_count,
        evaluate_situation_function=evaluate_situation_pandapower,
    )
