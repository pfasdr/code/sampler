from timeit import timeit


def test_benchmark_sampling():
    # Mock
    setup_mv_oberrhein_random = \
        'from pandapower import pandapowerNet; ' \
        'from pfasdr.grid_pandapower.model.draw_random_sample import draw_sample_random; ' \
        'from pfasdr.grid_pandapower.model.get_mv_oberrhein_net_empty import get_mv_oberrhein_net_empty; ' \
        'net: pandapowerNet = get_mv_oberrhein_net_empty()'

    # Test
    time_random = timeit(
        setup=setup_mv_oberrhein_random,
        stmt='validity: bool = draw_sample_random(net=net)',
        number=10,
    )

    # Mock
    setup_mv_oberrhein_preset =\
        'from pandapower import pandapowerNet; ' \
        'from pfasdr.grid_pandapower.model.draw_random_sample import draw_sample_random; ' \
        'from pfasdr.grid_pandapower.model.get_mv_oberrhein_net_preset import get_mv_oberrhein_net_preset; ' \
        'net: pandapowerNet = get_mv_oberrhein_net_preset()'

    # Test
    time_preset = timeit(
        setup=setup_mv_oberrhein_preset,
        stmt='validity: bool = draw_sample_random(net=net)',
        number=10,
    )

    # Assert
    assert time_random < time_preset * 1.2
    assert time_preset < time_random * 1.2
