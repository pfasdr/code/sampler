from pfasdr.grid_pandapower.virtual_file_system.ensure_data_file_module import \
    ensure_data_file
from pfasdr.grid_pandapower.virtual_file_system.\
    get_file_path_for_grid_type_and_node_count_module import \
    get_file_path_for_grid_type_and_node_count_and_validity


def test_ensure_data_file():
    for node_count in range(2, 4):
        # Mock
        filepath_valid = \
            get_file_path_for_grid_type_and_node_count_and_validity(
                node_count=node_count
            )

        # Test
        ensure_data_file(
            filepath=filepath_valid,
        )

    # Assert
    with open(file=filepath_valid) as data_file:
        first_row = data_file.readline().rstrip('\n')
    assert first_row != '<!DOCTYPE html>'
    assert first_row == \
        '0.13966888086414272,0.07176089046543738,0.09448961392423302'
