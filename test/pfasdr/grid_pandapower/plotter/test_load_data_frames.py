from pfasdr.grid_pandapower.virtual_file_system.load_data_frames import \
    load_data_frames


def test_load_data_frames():
    # Mock
    node_count = 2

    # Test
    data_frames = load_data_frames(node_count=node_count)

    # Assert
    assert data_frames
