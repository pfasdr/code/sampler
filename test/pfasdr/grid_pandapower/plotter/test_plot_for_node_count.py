from pfasdr.grid_pandapower.plotter.plot_for_node_count import \
    plot_for_node_count
from pfasdr.grid_pandapower.virtual_file_system.ensure_data_file_module import \
    ensure_data_file
from pfasdr.grid_pandapower.virtual_file_system.\
    get_file_path_for_grid_type_and_node_count_module import \
    get_file_path_for_grid_type_and_node_count_and_validity


def test_plot_for_node_count():
    node_count = 3
    filepath_valid = \
        get_file_path_for_grid_type_and_node_count_and_validity(
            grid_type='linear',
            node_count=node_count,
            validity='valid'
        )
    ensure_data_file(filepath=filepath_valid)

    plot_for_node_count(node_count=node_count)
