from pfasdr.grid_pandapower.sampler.sampler_pandapower.\
    evaluate_situation_pandapower_module import \
    evaluate_situation_pandapower
from pfasdr.grid_pandapower.sampler.sampler_pandapower.sample_panda_power \
    import sample_pandapower


def test_sample_pandapower():
    load_counts_pandapower = (100, )
    limit = 1

    sample_pandapower(
        evaluate_situation_function=evaluate_situation_pandapower,
        load_counts=load_counts_pandapower,
        limit=limit,
    )
