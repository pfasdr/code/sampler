from pandapower import pandapowerNet

from pfasdr.grid_pandapower.model.determine_validity_module import \
    determine_validity
from pfasdr.grid_pandapower.model.get_mv_oberrhein_net_preset import \
    get_mv_oberrhein_net_preset


def test_get_mv_oberrhein_model():
    # Test
    net: pandapowerNet = get_mv_oberrhein_net_preset()

    # Assert
    assert net
    assert isinstance(net, pandapowerNet)

    # Test
    validity: bool = determine_validity(net=net)

    # Assert
    assert isinstance(validity, bool)
