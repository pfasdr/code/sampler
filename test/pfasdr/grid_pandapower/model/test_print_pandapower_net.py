from pandapower import pandapowerNet

from pfasdr.grid_pandapower.model.get_mv_oberrhein_net_preset import \
    get_mv_oberrhein_net_preset
from pfasdr.grid_pandapower.model.print_net import \
    print_pandapower_net


def test_print_pandapower_net():
    # Test
    net: pandapowerNet = get_mv_oberrhein_net_preset()

    print_pandapower_net(net=net)

    # Assert
    assert True
