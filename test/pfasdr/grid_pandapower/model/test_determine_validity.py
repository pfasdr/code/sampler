import numpy
import pandapower
from pandapower import pandapowerNet

from pfasdr.grid_pandapower.model.determine_validity_module import \
    determine_validity
from pfasdr.grid_pandapower.model.get_mv_oberrhein_net_empty import \
    get_mv_oberrhein_net_empty
from pfasdr.grid_pandapower.model.get_mv_oberrhein_net_preset import \
    get_mv_oberrhein_net_preset
from pfasdr.grid_pandapower.model.scale_net_powers import scale_net_powers


def test_get_mv_oberrhein_model_empty():
    # Test
    net: pandapowerNet = get_mv_oberrhein_net_empty()

    validity = determine_validity(net=net)

    # Assert
    assert validity


def test_get_mv_oberrhein_model_preset():
    # Test
    net: pandapowerNet = get_mv_oberrhein_net_preset()

    validity = determine_validity(net=net)

    # Assert
    assert validity


def test_get_mv_oberrhein_model_invalid_overloaded_lines():
    # Test
    net: pandapowerNet = get_mv_oberrhein_net_empty()

    net = scale_net_powers(
        net=net,
        load_scaling=numpy.ones(shape=(147, 1)) * 0,
        sgen_scaling=numpy.ones(shape=(153, 1)) * 3,
    )

    pandapower.runpp(net)

    validity = determine_validity(net=net)

    # Assert
    assert not validity


def test_get_mv_oberrhein_model_invalid_violated_buses():
    # Test
    net: pandapowerNet = get_mv_oberrhein_net_empty()

    net = scale_net_powers(
        net=net,
        load_scaling=numpy.ones(shape=(147, 1)) * 0,
        sgen_scaling=numpy.ones(shape=(153, 1)) * 3,
    )

    pandapower.runpp(net)

    validity = determine_validity(net=net)

    # Assert
    assert not validity
