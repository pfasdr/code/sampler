from pandapower import pandapowerNet, runpp
import numpy
from pytest import raises

from pfasdr.grid_pandapower.model.configure_pandas_printing import \
    configure_pandas_printing
from pfasdr.grid_pandapower.model.get_mv_oberrhein_net_empty import \
    get_mv_oberrhein_net_empty
from pfasdr.grid_pandapower.model.scale_net_powers import \
    scale_net_powers
from pfasdr.grid_pandapower.model.print_net import \
    print_pandapower_net


def test_get_my_mv_oberrhein_model():
    # Test
    configure_pandas_printing()

    # Test
    net: pandapowerNet = get_mv_oberrhein_net_empty()

    # Assert
    assert net
    assert isinstance(net, pandapowerNet)

    print_pandapower_net(net=net)

    # Assert
    assert net
    assert isinstance(net, pandapowerNet)

    # Test
    net = scale_net_powers(
        net=net,
        load_scaling=numpy.random.uniform(0, 1, size=(147, 1)),
        sgen_scaling=numpy.random.uniform(0, 1, size=(153, 1)),
    )

    with raises(ValueError):
        # Test
        net = scale_net_powers(
            net=net,
            load_scaling=numpy.random.uniform(0, 1, size=(999, 1)),
            sgen_scaling=numpy.random.uniform(0, 1, size=(153, 1)),
        )

    with raises(ValueError):
        # Test
        net = scale_net_powers(
            net=net,
            load_scaling=numpy.random.uniform(0, 1, size=(147, 1)),
            sgen_scaling=numpy.random.uniform(0, 1, size=(999, 1)),
        )

    # Assert
    assert net
    assert isinstance(net, pandapowerNet)

    # Test
    print_pandapower_net(net=net)

    # Assert
    assert net
    assert isinstance(net, pandapowerNet)

    # Test
    runpp(net)

    # Assert
    assert net
    assert isinstance(net, pandapowerNet)

    # Test
    print_pandapower_net(net=net)

    # Assert
    assert net
    assert isinstance(net, pandapowerNet)
