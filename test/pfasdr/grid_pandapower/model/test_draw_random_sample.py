from pandapower import pandapowerNet

from pfasdr.grid_pandapower.model.draw_random_sample import draw_sample_random
from pfasdr.grid_pandapower.model.get_mv_oberrhein_net_empty import \
    get_mv_oberrhein_net_empty


def test_draw_sample_random():
    # Mock
    net: pandapowerNet = get_mv_oberrhein_net_empty()

    # Test
    validity: bool = draw_sample_random(net=net)

    # Assert
    assert isinstance(validity, bool)
